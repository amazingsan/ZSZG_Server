package com.begamer.card.json;

public class ItoolsPayJson
{
	public String order_id_com;//发起支付时的订单号
	public String user_id;//支付的用户id
	public String amount;//成功支付的金额
	public String account;//支付帐号
	public String order_id;//支付平台的订单号
	public String result;//支付结果, 目前只有成功状态, success
	
	public String getOrder_id_com()
	{
		return order_id_com;
	}
	public void setOrder_id_com(String orderIdCom)
	{
		order_id_com = orderIdCom;
	}
	public String getUser_id()
	{
		return user_id;
	}
	public void setUser_id(String userId)
	{
		user_id = userId;
	}
	public String getAmount()
	{
		return amount;
	}
	public void setAmount(String amount)
	{
		this.amount = amount;
	}
	public String getAccount()
	{
		return account;
	}
	public void setAccount(String account)
	{
		this.account = account;
	}
	public String getOrder_id()
	{
		return order_id;
	}
	public void setOrder_id(String orderId)
	{
		order_id = orderId;
	}
	public String getResult()
	{
		return result;
	}
	public void setResult(String result)
	{
		this.result = result;
	}
}
