package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RuneTotalData implements PropertyReader
{
	/**编号**/
	public int id;
	/**名称**/
	public String name;
	/**属性**/
	public int proprety;
	/**数值**/
	public int value;
	/**等级**/
	public int level;
	
	private static HashMap<Integer, RuneTotalData> data=new HashMap<Integer, RuneTotalData>();
	private static List<Integer> ids=new ArrayList<Integer>();
	@Override
	public void addData()
	{
		data.put(id, this);
		ids.add(id);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
		ids.clear();
	}

	public static RuneTotalData getData(int id)
	{
		return data.get(id);
	}
	
	/**
	 * 获取本轮的同proprety的value总和
	 * lt@2014-2-21 上午11:01:20
	 * @param id
	 * @param proprety
	 * @return
	 */
	public static int getValues(int times,int property)
	{
		int result=0;
		for(int i=0;i<ids.size();i++)
		{
			int idTemp=ids.get(i);
			if(idTemp/100==times)
			{
				RuneTotalData rtd=RuneTotalData.getData(idTemp);
				if(rtd.proprety==property)
				{
					result+=rtd.value;
					result+=RuneData.getValues(idTemp);
				}
			}
		}
		return result;
	}
	
}
