package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class RankData implements PropertyReader {
	/** 编号 **/
	public int level;
	public int number1;
	/** 随机掉落 **/
	public List<String> drops;
	/****/
	public int award;
	public int number;

	public int honorlose;
	public int honorwin;
	/** 排名奖励 **/
	public List<String> rank;
	private static HashMap<Integer, RankData> data = new HashMap<Integer, RankData>();
	private static final Logger errorlogger = ErrorLogger.logger;

	@Override
	public void addData() {
		data.put(level, this);
	}

	@Override
	public void parse(String[] ss) {
		int location = 0;
		level = StringUtil.getInt(ss[location]);
		number1 = StringUtil.getInt(ss[location + 1]);
		drops = new ArrayList<String>();
		for (int i = 0; i < 2; i++) {
			location = 2 + i * 3;
			int type = StringUtil.getInt(ss[location]);
			String drop = StringUtil.getString(ss[location + 1]);
			int odds = StringUtil.getInt(ss[location + 2]);
			try {
				if (!"".equals(drop) && odds > 0) {
					String[] temp = drop.split(",");
					int dropId = StringUtil.getInt(temp[0]);
					int num = StringUtil.getInt(temp[1]);
					String drop_odds = type + "-" + drop + "-" + odds;
					drops.add(drop_odds);
				}
			} catch (Exception e) {
				errorlogger.error("加载rank表随即掉落数据出错:" + level, e);
				System.exit(0);
			}
		}
		location = 2 + 2 * 3;
		award = StringUtil.getInt(ss[location]);
		number = StringUtil.getInt(ss[location + 1]);
		honorlose = StringUtil.getInt(ss[location + 2]);
		honorwin = StringUtil.getInt(ss[location + 3]);
		rank = new ArrayList<String>();
		for (int i = 0; i < 30; i++) {
			location = 2 + 2 * 3 + 4 + i * 5;
			int number1 = StringUtil.getInt(ss[location]);
			int type1 = StringUtil.getInt(ss[location + 1]);
			int award1 = StringUtil.getInt(ss[location + 2]);
			int type2 = StringUtil.getInt(ss[location + 3]);
			int award2 = StringUtil.getInt(ss[location + 4]);
			String ranks = number1 + "-" + award1+"-"+award2;
			if (number1 == 0) {
				continue;
			} else {
				rank.add(ranks);
			}

		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}

	/** 根据level获取一个rankdata **/
	public static RankData getRankdata(int lv) {
		return data.get(lv);
	}
}
