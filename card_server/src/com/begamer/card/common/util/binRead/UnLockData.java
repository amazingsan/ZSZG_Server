package com.begamer.card.common.util.binRead;


import java.util.HashMap;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.model.pojo.Activity;

public class UnLockData implements PropertyReader
{
	/**编号**/
	public int id;
	/**模块名称**/
	public String name;
	/**模块**/
	public int mode;
	/**描述**/
	public String description;
	/**解锁类型**/
	public int type;
	/**解锁条件**/
	public int method;
	public int showup;//是否弹窗
	//==解锁图标==//
	public String icon;
	//==解锁描述==//
	public String unlockdes;
	
	private static HashMap<Integer, UnLockData> data =new HashMap<Integer, UnLockData>();
	@Override
	public void addData() 
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) 
	{

	}

	@Override
	public void resetData() 
	{
		data.clear();
	}
	
	/**根据id获取unlockdata**/
	public static UnLockData getUnLockData(int unlockId)
	{
		if(data.get(unlockId) !=null)
		{
			return data.get(unlockId);
		}
		return null;
	}
	/**id-是否解锁(0未解锁,1关卡解锁,2等级解锁)**/
	public static String [] getUnLockStrByMethod(int mission , int lv)
	{
		String [] s =new String[data.size()];
		int i=0;
		for(UnLockData uData : data.values())
		{
			String str =uData.id+"-"+0;
			if(uData.type==0)//默认开启
			{
				str =uData.id+"-"+1;	
			}
			else if(uData.type==1)//关卡解锁
			{
				if(uData.method<=mission)
				{
					str =uData.id+"-"+1;
				}
			}
			else if(uData.type==2)//等级解锁
			{
				if(uData.method<=lv)
				{
					str =uData.id+"-"+1;
				}
			}
			else if(uData.type==4 && uData.id==42)
			{
				Activity a =Cache.getInstance().getActivityById(6);
				if(a !=null)
				{
					if(StringUtil.getDate(System.currentTimeMillis()).compareTo(a.getSttime())>=0 && StringUtil.getDate(System.currentTimeMillis()).compareTo(a.getEndtime())<=0)
					{
						str =uData.id+"-"+1;
					}
					else
					{
						str =uData.id+"-"+0;
					}
				}
				else
				{
					str =uData.id+"-"+0;
				}
			}
			s[i]=str;
			i++;
		}
		return s;
	}
	
	public static UnLockData getUnlockByMode(int index)
	{
		for(UnLockData uData : data.values())
		{
			if(uData.mode==index)
			{
				return uData;
			}
		}
		return null;
	}
}
