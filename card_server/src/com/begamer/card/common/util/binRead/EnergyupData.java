package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.Constant;

public class EnergyupData implements PropertyReader
{
	//==购买次数==//
	public int number;
	//==怒气数量==//
	public int energy;
	//==需求vip等级==//
	public int viplevel;
	//==花费水晶==//
	public int cost;
	
	private static HashMap<Integer, EnergyupData> data=new HashMap<Integer, EnergyupData>();
	private static List<EnergyupData> listData=new ArrayList<EnergyupData>();
	
	@Override
	public void addData()
	{
		data.put(number,this);
		listData.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
		listData.clear();
	}
	
	public static EnergyupData getData(int number)
	{
		return data.get(number);
	}
	
	//获取购买次数
	public static int getNumber(int maxEnergy)
	{
		int temp=maxEnergy-Constant.InitMaxEnergy;
		for(int i=0;i<listData.size();i++)
		{
			EnergyupData ed=listData.get(i);
			if(temp<ed.energy)
			{
				return ed.number-1;
			}
			else
			{
				temp-=ed.energy;
			}
		}
		return listData.get(listData.size()-1).number;
	}
}
