package com.begamer.card.model.view;

public class PassiveSkillView {

	private int id;
	private String name;
	private String describe;
	private int level;

	public static PassiveSkillView ceatePassiveSkillView(int id, String name,
			String describe, int level)
	{
		PassiveSkillView passiveSkillView = new PassiveSkillView();
		passiveSkillView.setId(id);
		passiveSkillView.setName(name);
		passiveSkillView.setDescribe(describe);
		passiveSkillView.setLevel(level);
		return passiveSkillView;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescribe()
	{
		return describe;
	}

	public void setDescribe(String describe)
	{
		this.describe = describe;
	}

}
