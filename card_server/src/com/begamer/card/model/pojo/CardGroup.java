package com.begamer.card.model.pojo;

import java.util.ArrayList;
import java.util.List;

import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.json.CardJson;
import com.begamer.card.json.SCEGJson;
import com.begamer.card.json.SCPSJson;

public class CardGroup {
	public Card[] cards;
	public Skill[] skills;
	public List<PassiveSkill>[] pSkills;
	public List<Equip>[] equips;
	public int unitId;
	
	public CardGroup(Card[] cards, Skill[] skills, List<PassiveSkill>[] pSkills, List<Equip>[] equips, int unitId)
	{
		this.cards = cards;
		this.skills = skills;
		this.pSkills = pSkills;
		this.equips = equips;
		this.unitId = unitId;
	}
	
	public Card[] getCards()
	{
		return cards;
	}
	
	public void setCards(Card[] cards)
	{
		this.cards = cards;
	}
	
	public Skill[] getSkills()
	{
		return skills;
	}
	
	public void setSkills(Skill[] skills)
	{
		this.skills = skills;
	}
	
	public List<PassiveSkill>[] getpSkills()
	{
		return pSkills;
	}
	
	public void setpSkills(List<PassiveSkill>[] pSkills)
	{
		this.pSkills = pSkills;
	}
	
	public List<Equip>[] getEquips()
	{
		return equips;
	}
	
	public void setEquips(List<Equip>[] equips)
	{
		this.equips = equips;
	}
	
	public int getUnitId()
	{
		return unitId;
	}
	
	public void setUnitId(int unitId)
	{
		this.unitId = unitId;
	}
	
	public void save(PlayerInfo pi, int[] cardIndexs, int[] skillIndexs, SCPSJson[] pSkillIndexs, SCEGJson[] equipIndexs, int unitId)
	{
		// card
		List<Card> cardList = pi.getCards();
		for (int i = 0; i < cardIndexs.length; i++)
		{
			int index = cardIndexs[i];
			if (index >= 0)
			{
				cards[i] = cardList.get(index);
			}
			else
			{
				cards[i] = null;
			}
		}
		// skill
		List<Skill> skillList = pi.getSkills();
		for (int i = 0; i < skillIndexs.length; i++)
		{
			int index = skillIndexs[i];
			if (index >= 0)
			{
				skills[i] = skillList.get(index);
			}
			else
			{
				skills[i] = null;
			}
		}
		// pskill
		List<PassiveSkill> pSkillList = pi.getPassiveSkillls();
		for (int i = 0; i < pSkillIndexs.length; i++)
		{
			SCPSJson scpsJson = pSkillIndexs[i];
			if (scpsJson == null || scpsJson.ps == null)
			{
				pSkills[i] = null;
			}
			else
			{
				pSkills[i] = new ArrayList<PassiveSkill>();
				for (int index : scpsJson.ps)
				{
					if (index >= 0 && index < pSkillList.size())
					{
						pSkills[i].add(pSkillList.get(index));
					}
					else
					{
						pSkills[i].add(null);
					}
				}
			}
		}
		// equips
		List<Equip> equipList = pi.getEquips();
		for (int i = 0; i < equipIndexs.length; i++)
		{
			SCEGJson scegj = equipIndexs[i];
			if (scegj == null || scegj.es == null)
			{
				equips[i] = null;
			}
			else
			{
				equips[i] = new ArrayList<Equip>();
				for (int index : scegj.es)
				{
					if (index >= 0 && index < equipList.size())
					{
						equips[i].add(equipList.get(index));
					}
				}
			}
		}
		// units
		this.unitId = unitId;
		
		pi.player.setIconId(unitId);
	}
	
	public CardJson[] getBattleData()
	{
		CardJson[] cjs = new CardJson[6];
		for (int i = 0; i < cjs.length; i++)
		{
			int cardId = 0;
			int level = 0;
			int skillId = 0;
			int skillLevel = 0;
			List<String> passiveSkillIds = new ArrayList<String>();
			List<String> equipInfos = new ArrayList<String>();
			if (cards[i] != null)
			{
				cardId = cards[i].getCardId();
				level = cards[i].getLevel();
			}
			else
			{
				continue;
			}
			if (skills[i] != null)
			{
				skillId = skills[i].getSkillId();
				skillLevel = skills[i].getLevel();
			}
			else
			{
				skillId = CardData.getData(cardId).basicskill;
				skillLevel = 1;
			}
			if (pSkills[i] != null)
			{
				List<PassiveSkill> pss = pSkills[i];
				for (PassiveSkill p : pss)
				{
					if (p!=null)
					{
						passiveSkillIds.add(p.getPassiveSkillId()+"");
					}
					else
					{
						passiveSkillIds.add(0+"");
					}
				}
			}
			if (equips[i] != null)
			{
				for (Equip e : equips[i])
				{
					equipInfos.add(e.getEquipId() + "-" + e.getLevel());
				}
			}
			cjs[i] = new CardJson(i, cardId, level, skillId, skillLevel, passiveSkillIds, equipInfos, cards[i].getBreakNum(), cards[i].getTalent1(), cards[i].getTalent2(), cards[i].getTalent3());
		}
		return cjs;
	}
	
	public int getPos(Card c)
	{
		for (int i = 0; i < cards.length; i++)
		{
			if (cards[i] == c)
			{
				return i;
			}
		}
		return -1;
	}
	
	public Card getCard(int pos)
	{
		if (cards != null && pos >= 0 && pos < cards.length)
		{
			return cards[pos];
		}
		return null;
	}
	
	public Skill getSkill(int pos)
	{
		if (skills != null && pos >= 0 && pos < skills.length)
		{
			return skills[pos];
		}
		return null;
	}
	
	public List<PassiveSkill> getPassiveSkills(int pos)
	{
		if (pSkills != null && pos >= 0 && pos < pSkills.length)
		{
			if (pSkills[pos] != null)
			{
				return pSkills[pos];
			}
		}
		return null;
	}
	
	public List<Equip> getEquips(int pos)
	{
		if (equips != null && pos >= 0 && pos < equips.length)
		{
			return equips[pos];
		}
		return null;
	}
	
	public boolean haveCard(Card card)
	{
		for (Card c : cards)
		{
			if (c == card)
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean haveSkill(Skill skill)
	{
		for (Skill s : skills)
		{
			if (s == skill)
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean havePskill(PassiveSkill pSkill)
	{
		for (List<PassiveSkill> s : pSkills)
		{
			if (s !=null && s.contains(pSkill))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean haveEquip(Equip equip)
	{
		for (List<Equip> es : equips)
		{
			if (es != null && es.contains(equip))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean canFight()
	{
		if (cards != null)
		{
			for (Card c : cards)
			{
				if (c != null)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public void gc()
	{
		cards = null;
		skills = null;
		pSkills = null;
		equips = null;
	}
	
}
