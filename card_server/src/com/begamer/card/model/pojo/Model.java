package com.begamer.card.model.pojo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Model implements Serializable{

	private int id;
	private int m_id;
	private String name;
	private String createOn;
	private int logo;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCreateOn()
	{
		return createOn;
	}

	public void setCreateOn(String createOn)
	{
		this.createOn = createOn;
	}

	public int getLogo()
	{
		return logo;
	}

	public void setLogo(int logo)
	{
		this.logo = logo;
	}

	public int getM_id()
	{
		return m_id;
	}

	public void setM_id(int m_id)
	{
		this.m_id = m_id;
	}

}
