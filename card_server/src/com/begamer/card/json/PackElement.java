package com.begamer.card.json;

public class PackElement {
	public int i;//索引
	public int eType;//1角色卡,2主动技能,3被动技能,4装备,5材料
	public int dataId;//excel表中的id
	public String ct;//创建时间
	public int lv;//等级
	public int curExp;
	public int bn;//突破次数
	public int use;//1在卡组,0不在
	public int pile;//叠加数
	
	//==只有此对象是角色卡时以下才有效==//
	public int skId;//技能Id
	
	public int nw;				//新卡牌//
	public int bp;//==战斗力==// 
	
	//==是否可突破
	public int bnType;//0不可突破，1可突破
	//突破进度
	public int breakType;
	public int newType;//是否为新卡(0为是，1为不是)
	
	
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getEType() {
		return eType;
	}
	public void setEType(int type) {
		eType = type;
	}
	public int getDataId() {
		return dataId;
	}
	public void setDataId(int dataId) {
		this.dataId = dataId;
	}
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	public int geteType()
	{
		return eType;
	}
	public void seteType(int eType)
	{
		this.eType = eType;
	}
	public int getCurExp()
	{
		return curExp;
	}
	public void setCurExp(int curExp)
	{
		this.curExp = curExp;
	}
	public int getBn()
	{
		return bn;
	}
	public void setBn(int bn)
	{
		this.bn = bn;
	}
	public int getUse()
	{
		return use;
	}
	public void setUse(int use)
	{
		this.use = use;
	}
	public int getSkId()
	{
		return skId;
	}
	public void setSkId(int skId)
	{
		this.skId = skId;
	}
	public int getBp()
	{
		return bp;
	}
	public void setBp(int bp)
	{
		this.bp = bp;
	}
	public int getBnType() {
		return bnType;
	}
	public void setBnType(int bnType) {
		this.bnType = bnType;
	}
	
	public int getBreakType() {
		return breakType;
	}
	public void setBreakType(int breakType) {
		this.breakType = breakType;
	}
	
	public int getNewType() {
		return newType;
	}
	public void setNewType(int newType) {
		this.newType = newType;
	}
	public void setCard(int index,int cardId,String createTime,int level,int curExp,int breakNum,int use,int skillId,int battlePower , int breakType ,int newType)
	{
		this.i=index;
		this.eType=1;
		this.dataId=cardId;
		this.ct=createTime;
		this.lv=level;
		this.curExp=curExp;
		this.bn=breakNum;
		this.use=use;
		this.skId=skillId;
		this.bp=battlePower;
		this.breakType =breakType;
		this.newType =newType;
	}
	
	public void setSkill(int index,int skillId,int level,String createTime,int curExp,int use ,int newType)
	{
		this.i=index;
		this.eType=2;
		this.dataId=skillId;
		this.ct=createTime;
		this.curExp=curExp;
		this.use=use;
		this.lv=level;
		this.newType =newType;
	}
	
	public void setPSkill(int index,int pSkillId,String createTime,int use ,int curExp ,int newType)
	{
		this.i=index;
		this.eType=3;
		this.dataId=pSkillId;
		this.ct=createTime;
		this.use=use;
		this.curExp =curExp;
		this.newType =newType;
	}
	
	public void setEquip(int index,int equipId,String createTime,int level,int use ,int newType)
	{
		this.i=index;
		this.eType=4;
		this.dataId=equipId;
		this.ct=createTime;
		this.lv=level;
		this.use=use;
		this.newType =newType;
	}
	
	public void setItem(int index,int itemId,int pile ,int newType)
	{
		this.i=index;
		this.eType=5;
		this.dataId=itemId;
		this.pile=pile;
		this.newType =newType;
	}
	public int getPile() {
		return pile;
	}
	public void setPile(int pile) {
		this.pile = pile;
	}
	public int getNw()
	{
		return nw;
	}
	public void setNw(int nw)
	{
		this.nw = nw;
	}
}
