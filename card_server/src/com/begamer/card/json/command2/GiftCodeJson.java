package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class GiftCodeJson extends BasicJson
{
	public String code;

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}
}
