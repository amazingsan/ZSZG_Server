package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class RuneResultJson extends ErrorJson
{
	/**当前符文信息:x-xx-xx-xx-xx-xx-xx,遍数-第1图点亮个数-第2图点亮个数-第3图点亮个数-第4图点亮个数-第5图点亮个数-第6图点亮个数**/
	public String id;
	/**当前符文值**/
	public int num;
	/**当前符文成功率增加值**/
	public String pro;
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public int getNum()
	{
		return num;
	}
	public void setNum(int num)
	{
		this.num = num;
	}
	public String getPro()
	{
		return pro;
	}
	public void setPro(String pro)
	{
		this.pro = pro;
	}
	
}
