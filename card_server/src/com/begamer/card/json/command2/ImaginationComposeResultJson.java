package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class ImaginationComposeResultJson extends ErrorJson
{
	/**兑换物品的id**/
	public int id;
	/**碎片信息**/
	public List<PackElement> s;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<PackElement> getS() {
		return s;
	}
	public void setS(List<PackElement> s) {
		this.s = s;
	}
	
}
