package com.begamer.card.json.command2;

import com.begamer.card.json.CardJson;
import com.begamer.card.json.ErrorJson;

public class MazeBattleResultJson extends ErrorJson {
	public int td;//迷宫编号id//
	public int md;//战斗关卡id
	public int state;//当前位置信息
	public int type;//普通战斗1  boss为2
	public int aF;//addFriendValue//
	public CardJson[] cs0;//cards0//
	public CardJson[] cs1;//cards1//
	public int[] us0;//unitSkillsId0//
	public int[] us1;//unitSkillsId1//
	public int bNum;//战斗场次
	public String [] s;//战斗掉落    type-id,num
	public int[] mes;//==双方最大怒气上限==//
	public String[] raceAtts;//==己方种族加成属性==//
	public int initE;//==初始怒气==//
	
	public int getTd() {
		return td;
	}
	public void setTd(int td) {
		this.td = td;
	}
	public int getAF() {
		return aF;
	}
	public void setAF(int af) {
		aF = af;
	}
	public CardJson[] getCs0() {
		return cs0;
	}
	public void setCs0(CardJson[] cs0) {
		this.cs0 = cs0;
	}
	public CardJson[] getCs1() {
		return cs1;
	}
	public void setCs1(CardJson[] cs1) {
		this.cs1 = cs1;
	}
	public int[] getUs0() {
		return us0;
	}
	public void setUs0(int[] us0) {
		this.us0 = us0;
	}
	public int[] getUs1() {
		return us1;
	}
	public void setUs1(int[] us1) {
		this.us1 = us1;
	}
	public int getBNum() {
		return bNum;
	}
	public void setBNum(int num) {
		bNum = num;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String[] getS() {
		return s;
	}
	public void setS(String[] s) {
		this.s = s;
	}
	public int getMd() {
		return md;
	}
	public void setMd(int md) {
		this.md = md;
	}
	public int[] getMes()
	{
		return mes;
	}
	public void setMes(int[] mes)
	{
		this.mes = mes;
	}
	public String[] getRaceAtts()
	{
		return raceAtts;
	}
	public void setRaceAtts(String[] raceAtts)
	{
		this.raceAtts = raceAtts;
	}
	public int getInitE()
	{
		return initE;
	}
	public void setInitE(int initE)
	{
		this.initE = initE;
	}
}
