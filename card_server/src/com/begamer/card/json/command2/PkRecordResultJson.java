package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.PkRecordElement;

public class PkRecordResultJson extends ErrorJson
{
	public List<PkRecordElement> list;//对战记录

	public List<PkRecordElement> getList() {
		return list;
	}

	public void setList(List<PkRecordElement> list) {
		this.list = list;
	}
	
}
