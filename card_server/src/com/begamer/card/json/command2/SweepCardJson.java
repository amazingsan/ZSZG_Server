package com.begamer.card.json.command2;

import java.util.List;

public class SweepCardJson
{
	public int playerExp;//军团经验
	public int cardExp;//人物经验
	public List<String> ds;//掉落物品   id-type
	public int getPlayerExp() {
		return playerExp;
	}
	public void setPlayerExp(int playerExp) {
		this.playerExp = playerExp;
	}
	public int getCardExp() {
		return cardExp;
	}
	public void setCardExp(int cardExp) {
		this.cardExp = cardExp;
	}
	public List<String> getDs() {
		return ds;
	}
	public void setDs(List<String> ds) {
		this.ds = ds;
	}
	
}
