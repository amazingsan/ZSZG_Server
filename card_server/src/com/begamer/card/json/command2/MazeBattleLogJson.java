package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class MazeBattleLogJson extends BasicJson{
	public int type;//1 普通战 2boss战//
	public int map;//迷宫id//
	public List<String> bs;//==合体技Id(0普通技能)-放技能前双方血量(&号分割12个血量)-放技能后双方血量(&号分割12个血量)-释放技能者index-伤害(&号分割若干个伤害)-当前回合数-合体技ids(&分割)==//
	public int r;//result:1胜利,2失败//
	public String gm;//goldMul金币倍数//
	public int bNum;//战斗场次
	public String cb;//卡牌-血,cardid-blood&cardid-blood
	
	public List<String> getBs()
	{
		return bs;
	}
	public void setBs(List<String> bs)
	{
		this.bs = bs;
	}
	public int getR()
	{
		return r;
	}
	public void setR(int r)
	{
		this.r = r;
	}
	public String getGm()
	{
		return gm;
	}
	public void setGm(String gm)
	{
		this.gm = gm;
	}
	public int getBNum()
	{
		return bNum;
	}
	public void setBNum(int num)
	{
		bNum = num;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getMap() {
		return map;
	}
	public void setMap(int map) {
		this.map = map;
	}
	public String getCb()
	{
		return cb;
	}
	public void setCb(String cb)
	{
		this.cb = cb;
	}
	
}
