package com.begamer.card.json.element;

public class MailUIResultElement
{
	//==邮件名字-发件人-发件时间(yyyy-MM-dd HH:ss)-是否新邮件(新邮件标识为1)==//
	public String title;
	public String sender;
	public String sendTime;
	public int mark;
	
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getSender()
	{
		return sender;
	}
	public void setSender(String sender)
	{
		this.sender = sender;
	}
	public String getSendTime()
	{
		return sendTime;
	}
	public void setSendTime(String sendTime)
	{
		this.sendTime = sendTime;
	}
	public int getMark()
	{
		return mark;
	}
	public void setMark(int mark)
	{
		this.mark = mark;
	}
}
